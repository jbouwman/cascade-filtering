## History of versions

**version 0.9.1**
- First online release

**version 0.9.3**
- Completely functional verison
- Changed dependencies in setup.py

**version 0.9.4**
- Added filter for SIFT

**version 1.0.0**
- Added fully functional edge filter
- Added source detection and mask creation
- Added new example notebooks and updated the excisting ones

**version 1.0.1**
- Made adjustment to source detection to remove small spurious structures
